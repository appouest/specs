Pod::Spec.new do |s|
  s.name             = "AOFramework+Swift"
  s.version          = "0.0.1"
  s.summary          = "AppOuest Framework + Swift"
  s.homepage         = "https://bitbucket.org/appouest/aoframeworkSwift"
  s.license = { 
                :type => 'Apache 2',
                :file => 'LICENSE' 
              }

  s.authors = { 'Aymeric De Abreu' => 'ada@appouest.com',
                'Jean-Baptiste Denoual' => 'jbd@appouest.com',
                'Flavien Simon' => 'flavien@appouest.com'
               }
  s.source           = { 
                          :git => "http://git@sources.appouest.com:10080/appouest-iOS/AOFrameworkSwift.git", :tag => s.version.to_s
                        }
  s.social_media_url = 'https://twitter.com/appouest'

  s.ios.deployment_target = '8.0'
  s.requires_arc = true

  s.dependency 'AOFramework'

  s.frameworks = 'UIKit'
  s.module_name = 'AOFrameworkSwift'


  s.subspec 'UIKit' do |sub|
    sub.source_files = 'Classes/UIKit/*.{swift}'
  end

end