#
# Be sure to run `pod lib lint AOUtils.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AOUtils'
  s.version          = '0.1.17'
  s.summary          = 'AOUtils is a framework created by AppOuest to mutualise code between iOS projects.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This is a framework to mutualise code between iOS projects.
                       DESC

  s.homepage         = 'https://bitbucket.org/appouest/aoutils'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Aymeric De Abreu' => 'ada@appouest.com' }
  s.source           = { :git => 'ssh://git@bitbucket.org/appouest/aoutils.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'AOUtils/Classes/**/*'
  
  # s.resource_bundles = {
  #   'AOUtils' => ['AOUtils/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'RealmSwift', '~> 2.0'
  s.dependency 'RxSwift', '~> 3.0'
  s.dependency 'SwiftyJSON'

end
