#
# Be sure to run `pod lib lint AOUtils.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AOUtils'
  s.version          = '1.4.6'
  s.summary          = 'AOUtils is a framework created by AppOuest to mutualise code between iOS projects.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This is a framework to mutualise code between iOS projects.
                       DESC

  s.homepage         = 'https://bitbucket.org/appouest/aoutils'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.authors = { 'Aymeric De Abreu' => 'ada@appouest.com',
                'Jean-Baptiste Denoual' => 'jbd@appouest.com',
                'Flavien Simon' => 'flavien@appouest.com'
               }
  s.source           = { :git => 'ssh://git@bitbucket.org/appouest/aoutils.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'
  s.osx.deployment_target  = '10.10'

  s.default_subspec = 'Core'
  # s.resource_bundles = {
  #   'AOUtils' => ['AOUtils/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'

  s.subspec 'Core' do |sub|
    sub.source_files = 'AOUtils/Classes/Foundation/*',  'AOUtils/Classes/StoreKit/*'
    sub.frameworks = 'Foundation', 'StoreKit'
  end

  s.subspec 'UIKit' do |sub|
    sub.source_files = 'AOUtils/Classes/UIKit/*'
    sub.frameworks = 'UIKit'
    sub.dependency 'AOUtils/Core'
    sub.platform = :ios
  end

  s.subspec 'Realm' do |sub|
    sub.source_files = 'AOUtils/Classes/Realm/*'
    sub.dependency 'RealmSwift', '~> 3.5'
    sub.dependency 'AOUtils/Core'
    sub.frameworks = 'Foundation'
  end

  s.subspec 'Network' do |sub|
    sub.frameworks = 'Foundation'
    sub.ios.frameworks = 'MobileCoreServices'
    sub.osx.frameworks = 'CoreServices'
    sub.dependency 'SwiftyJSON', '~> 4.1'
    sub.dependency 'AOUtils/Core'
    sub.source_files = 'AOUtils/Classes/Network/*'
  end

  s.subspec 'RxNetwork' do |sub|
    sub.source_files = 'AOUtils/Classes/Network/Rx/*'
    sub.dependency 'RxSwift', '~> 3.0'
    sub.dependency 'AOUtils/Network'
  end

  s.subspec 'PromiseNetwork' do |sub|
      sub.source_files = 'AOUtils/Classes/Network/Promise/*'
      sub.dependency 'PromiseKit/CorePromise', '~> 6.2'
      sub.dependency 'AOUtils/Network'
  end 

  s.subspec 'SwiftGen' do |sub|
    sub.source_files = 'AOUtils/Classes/SwiftGen/*'
    sub.dependency 'SwiftGen', '~> 5.3'
    sub.preserve_path = 'AOUtils/Resources/SwiftGenTemplates/colors-custom.stencil'
  end

end
