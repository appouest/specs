Pod::Spec.new do |s|
  s.name             = "AOFramework"
  s.version          = "0.1.10"
  s.summary          = "AppOuest Framework"
  s.homepage         = "https://bitbucket.org/appouest/aoframework"
  s.license = { 
                :type => 'Apache 2',
                :file => 'LICENSE' 
              }

  s.authors = { 'Aymeric De Abreu' => 'ada@appouest.com',
                'Jean-Baptiste Denoual' => 'jbd@appouest.com',
                'Flavien Simon' => 'flavien@appouest.com'
               }
  s.source           = { 
                          :git => "ssh://git@bitbucket.org/appouest/aoframework.git", :tag => s.version.to_s
                        }
  s.social_media_url = 'https://twitter.com/appouest'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Classes/*.{h,m}'

  s.frameworks = 'UIKit'
  s.module_name = 'AOFramework'

  s.subspec 'Foundation' do |sub|
    sub.source_files = 'Classes/Foundation/*.{h,m}'
  end

  s.subspec 'UIKit' do |sub|
    sub.dependency 'SVProgressHUD'
    sub.dependency 'DQAlertView'
    sub.source_files = 'Classes/UIKit/*.{h,m}'
  end

  s.subspec 'Assets' do |sub|
    sub.source_files = 'Classes/Assets/*.{h,m}'
    sub.ios.framework = 'MobileCoreServices' , 'AssetsLibrary' , 'Photos'
  end

  s.subspec 'Components' do |sub|
    sub.subspec 'SlideOver' do |slideover|
      slideover.dependency 'AOFramework/UIKit'
      slideover.source_files = 'Classes/Components/SlideOver/*.{h,m}'
    end
  end

end