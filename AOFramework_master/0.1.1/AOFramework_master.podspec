Pod::Spec.new do |s|
  s.name             = "AOFramework_master"
  s.version          = "0.1.1"
  s.summary          = "AppOuest Framework"
  s.homepage         = "https://bitbucket.org/appouest/aoframework"
  s.license = { 
                :type => 'Apache 2',
                :file => 'LICENSE' 
              }

  s.authors = { 'Aymeric De Abreu' => 'ada@appouest.com',
                'Jean-Baptiste Denoual' => 'jbd@appouest.com',
                'Flavien Simon' => 'flavien@appouest.com'
               }
  s.source           = { :git => "ssh://git@bitbucket.org/appouest/aoframework.git"}
  s.social_media_url = 'https://twitter.com/appouest'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Classes/**/*.{h,m}'

  s.frameworks = 'UIKit'
  s.module_name = 'AOFramework_master'

end